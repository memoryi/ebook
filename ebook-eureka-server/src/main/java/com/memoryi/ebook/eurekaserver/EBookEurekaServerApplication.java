package com.memoryi.ebook.eurekaserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@EnableEurekaServer
public class EBookEurekaServerApplication {

	private static Logger logger = LoggerFactory.getLogger(EBookEurekaServerApplication.class);
	
	public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(EBookEurekaServerApplication.class, args);
        logger.info("=======EBOOK EUREKA SERVER START=======");
    }
}
