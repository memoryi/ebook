package com.memoryi.ebook.configserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@EnableConfigServer
public class EBookConfigServerApplication {

	private static Logger logger = LoggerFactory.getLogger(EBookConfigServerApplication.class);
	
	public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(EBookConfigServerApplication.class, args);
        logger.info("=======EBOOK CONFIG SERVER START=======");
    }
}
