package com.memoryi.ebook.turbineserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.turbine.EnableTurbine;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@EnableTurbine
@EnableEurekaClient
public class EBookTurbineServerApplication {

	private static Logger logger = LoggerFactory.getLogger(EBookTurbineServerApplication.class);
	
	public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(EBookTurbineServerApplication.class, args);
        logger.info("=======EBOOK TURBINE SERVER START=======");
    }
}
