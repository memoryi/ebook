package com.memoryi.ebook.user.controller.sample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.memoryi.ebook.user.EBookUserApplication;

@RestController
public class RestSampleController {
	private static Logger logger = LoggerFactory.getLogger(RestSampleController.class);
	
	@RequestMapping("/userSample")
    public String hystrixSample() throws Exception {
		logger.info("--------->");
		return "Hello World";
    }
}
