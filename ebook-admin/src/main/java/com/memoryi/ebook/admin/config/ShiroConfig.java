package com.memoryi.ebook.admin.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource({ "classpath:spring/spring-shiro.xml"})
public class ShiroConfig{

}
