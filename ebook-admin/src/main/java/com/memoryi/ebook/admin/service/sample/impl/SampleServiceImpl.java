package com.memoryi.ebook.admin.service.sample.impl;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.memoryi.ebook.admin.EBookAdminApplication;
import com.memoryi.ebook.admin.service.sample.FeignSampleService;
import com.memoryi.ebook.admin.service.sample.SampleService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class SampleServiceImpl implements SampleService{

	private static Logger logger = LoggerFactory.getLogger(SampleServiceImpl.class);
	
	@Resource
	private RestTemplate restTemplate;
	
	@Resource
	private FeignSampleService feignsampleService;
	
	@Override
	@HystrixCommand(fallbackMethod="queryUsersHystrix")
	public String queryUsers() throws Exception {
		return restTemplate.getForObject("http://ebook-user/userSample", String.class);
	}
	
	@Override
	@HystrixCommand(fallbackMethod="queryUsersHystrix")
	public String queryUsersFeign() throws Exception {
		logger.info("------queryUsersFeign---------");
		return feignsampleService.queryUsers();
	}
	
	public String queryUsersHystrix(){
	    return "Error";
	}


}
