package com.memoryi.ebook.admin.controller.sample;

import org.apache.shiro.SecurityUtils;
import org.bitbucket.memoryi.ext.shiro.cache.FrameworkQueryCacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SampleController {

	@Autowired
	private FrameworkQueryCacheManager frameworkQueryCacheManager;
	
	@RequestMapping("/velocity")
    public String velocity(String id, Model model) {
		model.addAttribute("id", id);
		model.addAttribute("user", frameworkQueryCacheManager.getCurrentInfo(SecurityUtils.getSubject().getPrincipals()));
        return "sample";
    }
	
	@RequestMapping("/clearCache")
    public void clearCache() {
		frameworkQueryCacheManager.clearCurrentInfo(SecurityUtils.getSubject().getPrincipals());
    }
	
}
