package com.memoryi.ebook.admin.service.user;

import java.util.List;

import com.memoryi.ebook.model.admin.User;

public interface UserService {

	public List<User> queryUsers(User user) throws Exception;
}
