package com.memoryi.ebook.admin.mapper.user;

import java.sql.SQLException;
import java.util.List;

import com.memoryi.ebook.model.admin.User;


public interface UserMapper {

	public List<User> queryUsers(User user) throws SQLException;
	
}
