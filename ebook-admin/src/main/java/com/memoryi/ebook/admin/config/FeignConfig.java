package com.memoryi.ebook.admin.config;

import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages="com.memoryi.ebook.admin")
public class FeignConfig {
}
