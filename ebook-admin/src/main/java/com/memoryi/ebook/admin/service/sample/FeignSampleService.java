package com.memoryi.ebook.admin.service.sample;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("ebook-user")
public interface FeignSampleService {

	@RequestMapping(value = "/userSample", method = RequestMethod.GET)
	public String queryUsers() throws Exception;
}
