package com.memoryi.ebook.admin.shiro;

import org.apache.shiro.subject.PrincipalCollection;
import org.bitbucket.memoryi.ext.shiro.realm.FrameworkAuthorizingRealm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;

public class EbookShiroAuthorizingRealm extends FrameworkAuthorizingRealm{

	private static final Logger log = LoggerFactory.getLogger(EbookShiroAuthorizingRealm.class);
	
	@Override
	@CacheEvict(value="admin-shiro",allEntries=true)
	public void clearCurrentInfo(PrincipalCollection principals) {
		log.info("cache has bean cleared");
	}
}
