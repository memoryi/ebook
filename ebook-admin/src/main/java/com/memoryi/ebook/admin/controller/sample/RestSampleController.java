package com.memoryi.ebook.admin.controller.sample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.memoryi.ebook.admin.service.sample.FeignSampleService;
import com.memoryi.ebook.admin.service.sample.SampleService;

@RestController
public class RestSampleController {

	@Autowired
	private SampleService sampleService;
	
	@Autowired
	private FeignSampleService feignsampleService;
	
	@RequestMapping("/hystrixSample")
    public String hystrixSample() throws Exception {
		String users =  sampleService.queryUsers();
		return users;
    }
	
	@RequestMapping("/feignSample")
    public String feignSample() throws Exception {
		String users =  feignsampleService.queryUsers();
		return users;
    }
	
	@RequestMapping("/feignSampleImpl")
    public String feignSampleImpl() throws Exception {
		String users =  sampleService.queryUsersFeign();
		return users;
    }
}
