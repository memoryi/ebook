package com.memoryi.ebook.admin.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan(value = "com.memoryi.ebook.admin.mapper")
public class MybatisConfig {
}
