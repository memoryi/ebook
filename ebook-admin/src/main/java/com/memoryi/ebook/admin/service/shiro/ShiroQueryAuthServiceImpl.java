package com.memoryi.ebook.admin.service.shiro;

import java.sql.SQLException;
import java.util.List;

import org.bitbucket.memoryi.ext.shiro.manager.FrameworkQueryManager;
import org.bitbucket.memoryi.ext.shiro.model.authority.Permission;
import org.bitbucket.memoryi.ext.shiro.model.authority.Role;
import org.bitbucket.memoryi.ext.shiro.model.authority.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;

import com.memoryi.ebook.admin.mapper.shiro.ShiroQueryAuthMapper;

@Transactional(rollbackFor=Exception.class)
public class ShiroQueryAuthServiceImpl implements FrameworkQueryManager{

	@Autowired
	private ShiroQueryAuthMapper shiroQueryAuthMapper;
	
	@Cacheable(value="admin-shiro", key="'ShiroQueryAuthServiceImpl-getRoleInfo-'+#userName")
	public User getUserInfo(String userName) throws SQLException {
		return shiroQueryAuthMapper.getUserInfo(userName);
	}

	@Cacheable(value="admin-shiro", key="'ShiroQueryAuthServiceImpl-getRoleInfo-'+#user.getUserId()")
	public List<Role> getRoleInfo(User user) throws SQLException {
		return shiroQueryAuthMapper.getRoleInfo(user);
	}

	public List<Permission> getPermissionInfo(List<Role> roles)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
