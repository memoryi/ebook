package com.memoryi.ebook.admin.controller.user;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.memoryi.ebook.model.admin.User;

@Controller
public class UserController {

	@RequestMapping("/user/list.html")
    public String index(User user) {
        return "userList";
    }
	
}
