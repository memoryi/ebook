package com.memoryi.ebook.admin.mapper.shiro;

import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.bitbucket.memoryi.ext.shiro.model.authority.Role;
import org.bitbucket.memoryi.ext.shiro.model.authority.User;

public interface ShiroQueryAuthMapper {

	@Select("SELECT id userId, user_name userNm, pass_word userPwd FROM t_admin_user user WHERE user.user_name = #{userName}") 
	public User getUserInfo(@Param("userName")String userName) throws SQLException;
	
	@Select("SELECT userrole.role_id roleId, role.role_name roleNm FROM t_admin_user_role userrole, t_admin_role role WHERE userrole.role_id = role.id and userrole.user_id = #{user.userId}") 
	public List<Role> getRoleInfo(@Param("user")User user) throws SQLException;
}
