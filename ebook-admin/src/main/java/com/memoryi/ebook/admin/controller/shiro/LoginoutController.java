package com.memoryi.ebook.admin.controller.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginoutController {

	@RequestMapping("/login")
    public String login(String userName, String password) {
		
		Subject curMember = SecurityUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken(userName, password);
		
		try {
			curMember.login(token);

		} catch (UnknownAccountException accountException) {
		} catch (LockedAccountException lockedAccountException) {
		} catch (IncorrectCredentialsException credentialsException) {
		} catch (ExcessiveAttemptsException attemptsException) {
		}
        return curMember.getSession().getId().toString();
    }
	
	@RequestMapping("/testlogin")
    public String testlogin() {
        return "login now!";
    }
//	@Autowired
//	private MallSettingService mallSettingService;
	
//    @RequestMapping(value="/mybatis/", method=RequestMethod.PUT)
//    public ResultInfo getAccountInfoByMybatis(@RequestBody TMoneyMerFund money) throws Exception {
//    	
//    	ResultInfo resultInfo = new ResultInfo(WebConstants.WebLoginError.SERVER.value);
//    	
//    	sampleProvider.updateByMemberIdWithMybatis(money);
//    	resultInfo.setSuccess(true);
//    	resultInfo.setCode(1);
//    	return resultInfo;
//    }
}
