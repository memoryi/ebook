package com.memoryi.ebook.admin.controller.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.memoryi.ebook.admin.service.user.UserService;
import com.memoryi.ebook.model.admin.User;

@RestController
@RefreshScope
public class UserRestController {

	@Value("${common.page.pageNumber}")
    private String pageNumber;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/admin/users.do", method = RequestMethod.GET)
    public List<User> users(User user) throws Exception{
        return userService.queryUsers(user);
    }
	
	@RequestMapping(value="/admin/pageNumber.do", method = RequestMethod.GET)
    public String pageNumber() throws Exception{
        return pageNumber;
    }
	
	@RequestMapping(value="/admin/post.do", method = RequestMethod.POST)
    public String post() throws Exception{
        return pageNumber;
    }
}
