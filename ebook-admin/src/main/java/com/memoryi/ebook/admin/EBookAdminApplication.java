package com.memoryi.ebook.admin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
//@PropertySource({ "classpath:properties/${spring.profiles.active}/config-${spring.profiles.active}.properties" })
public class EBookAdminApplication {
	private static Logger logger = LoggerFactory.getLogger(EBookAdminApplication.class);

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(EBookAdminApplication.class, args);
		logger.info("======= EBOOK ADMIN SERVER START=======");
	}
}
