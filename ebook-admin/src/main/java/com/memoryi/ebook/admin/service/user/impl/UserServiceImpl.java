package com.memoryi.ebook.admin.service.user.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memoryi.ebook.admin.mapper.user.UserMapper;
import com.memoryi.ebook.admin.service.user.UserService;
import com.memoryi.ebook.model.admin.User;

@Transactional(rollbackFor=Exception.class)
@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserMapper userMapper;
	
	@Override
	public List<User> queryUsers(User user) throws Exception {
		return userMapper.queryUsers(user);
	}

}
