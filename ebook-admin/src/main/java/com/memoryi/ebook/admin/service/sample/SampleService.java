package com.memoryi.ebook.admin.service.sample;

public interface SampleService {

	public String queryUsers() throws Exception;
	
	public String queryUsersFeign() throws Exception;
}
