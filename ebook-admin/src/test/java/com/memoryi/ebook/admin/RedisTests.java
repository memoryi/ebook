package com.memoryi.ebook.admin;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.BoundSetOperations;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(EBookAdminApplication.class)
@WebIntegrationTest({"spring.profiles.active=dev"})
public class RedisTests {

	@Autowired
	private RedisTemplate<String, String> redisTemplate;
	
	@Test
	public void redisStringTest() throws Exception {
		BoundValueOperations<String, String> ops = redisTemplate.boundValueOps("springboot:redis:test:string");
		
		ops.set("test String");
		
		System.out.println("Hello World!"+ops.get());
	}
	
	@Test
	public void redisHashTest() throws Exception {
		BoundHashOperations<String,Object,Object> ops = redisTemplate.boundHashOps("springboot:redis:test:hash");
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("username", "jack");
		ops.putAll(obj);
		
	}
	
	@Test
	public void redisListTest() throws Exception {
		BoundListOperations<String, String> ops = redisTemplate.boundListOps("springboot:redis:test:list");
//		Map<String, Object> obj = new HashMap<String, Object>();
//		obj.put("username", "jack");
//		ops.putAll(obj);
		
		ops.leftPush("left");
		
	}
	
	@Test
	public void redisSetTest() throws Exception {
		BoundSetOperations<String, String> ops = redisTemplate.boundSetOps("springboot:redis:test:set");
		ops.add("set1", "set1");
	}
	
	@Test
	@Transactional(rollbackFor=Exception.class)
	public void redisRoleTest() throws Exception {
		BoundHashOperations<String,Object,Object> idOps = redisTemplate.boundHashOps("ebook:admin:increment");
		Long roleId = idOps.increment("roleId", 1);
		BoundHashOperations<String,Object,Object> userOps = redisTemplate.boundHashOps("ebook:admin:role:"+roleId);
		Map<String, Object> role1 = new HashMap<String, Object>();
		role1.put("roleName", "admin");
		userOps.putAll(role1);
		
		Long roleId2 = idOps.increment("roleId", 1);
		BoundHashOperations<String,Object,Object> userOps2 = redisTemplate.boundHashOps("ebook:admin:role:"+roleId2);
		
		Map<String, Object> role2 = new HashMap<String, Object>();
		role2.put("roleName", "guest");
		userOps2.putAll(role2);
	}
	
	@Test
	@Transactional(rollbackFor=Exception.class)
	public void redisUserTest() throws Exception {
		BoundHashOperations<String,Object,Object> idOps = redisTemplate.boundHashOps("ebook:admin:increment");
		Long userId = idOps.increment("userId", 1);
		BoundHashOperations<String,Object,Object> userOps = redisTemplate.boundHashOps("ebook:admin:user:"+userId);
		Map<String, Object> user = new HashMap<String, Object>();
		user.put("userName", "ffff121");
		user.put("age", "32");
		user.put("realName", "jack");
		userOps.putAll(user);
		
		
		BoundSetOperations<String, String> ops = redisTemplate.boundSetOps("ebook:admin:user:roles:"+userId);
		ops.add("2", "3");
		
		BoundSetOperations<String, String> roleOps1 = redisTemplate.boundSetOps("ebook:admin:role:users:2");
		roleOps1.add(userId+"");

		BoundSetOperations<String, String> roleOps2 = redisTemplate.boundSetOps("ebook:admin:role:users:3");
		roleOps2.add(1+"");
	}
	
}
