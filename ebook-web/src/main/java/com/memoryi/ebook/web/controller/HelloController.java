package com.memoryi.ebook.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.memoryi.ebook.web.service.SampleService;

@RestController
public class HelloController {

	private static Logger logger = LoggerFactory.getLogger(HelloController.class);
	
	@Autowired
	private SampleService sampleService;
	
    @RequestMapping("/")
    public String index(String id) {
    	sampleService.queryById("f7a53221-2aa3-4753-907b-9e36e575a8d9");
        return "i love memoryi";
    }
    
    @RequestMapping("/testtoken")
    public String testtoken(String signature, String timestamp, String nonce, String echostr) {
    	logger.info("test signature: {}, timestamp: {}, nonce: {}, token: {}", signature, timestamp, nonce, echostr);
        return echostr;
    }

    @RequestMapping("/sample")
    public String sample(String id) {
        return sampleService.queryById(id).getName()+", "+"i love memoryi";
    }
}
