package com.memoryi.ebook.web.common;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class EBookExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(EBookExceptionHandler.class);

	@ExceptionHandler(Throwable.class)
	public @ResponseBody Map<String, String> handleException(HttpServletRequest request, Throwable ex) {
		Map<String, String> result = new HashMap<String,String>();
		logger.info("Exception Occured:: URL=" + request.getRequestURL(), ex);
		result.put("message","请求出错!");
		result.put("success", "false");
		return result;
	}
}
