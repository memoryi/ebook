package com.memoryi.ebook.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
//@PropertySource({"classpath:properties/${spring.profiles.active}/config-${spring.profiles.active}.properties"
//})
public class EBookWebApplication extends SpringBootServletInitializer {

	private static Logger logger = LoggerFactory.getLogger(EBookWebApplication.class);
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(EBookWebApplication.class);
    }
	
	public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(EBookWebApplication.class, args);
        logger.info("=======EBOOK WEB SERVER START=======");
    }
}
