package com.memoryi.ebook.web.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.boot.actuate.metrics.Metric;
import org.springframework.boot.actuate.metrics.repository.MetricRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memoryi.ebook.web.mapper.SampleMapper;
import com.memoryi.ebook.web.model.Sample;
import com.memoryi.ebook.web.service.SampleService;

@Transactional(rollbackFor=Exception.class)
@Service
public class SampleServiceImpl implements SampleService{

	private static final Logger logger = LoggerFactory.getLogger(SampleServiceImpl.class);
	@Autowired
	private CounterService counterService;
	
	@Autowired
	private MetricRepository repository;
	
	@Autowired
	private SampleMapper sampleMapper;
	
	@Override
	public Sample queryById(String id) {
		counterService.increment("counter.calls.queryById");
		
		for(Metric m : repository.findAll()){
			logger.info(m.getName(),m.getValue());
		}
		return sampleMapper.queryById(id);
	}

}
