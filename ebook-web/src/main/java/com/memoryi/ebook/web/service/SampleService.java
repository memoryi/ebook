package com.memoryi.ebook.web.service;

import com.memoryi.ebook.web.model.Sample;

public interface SampleService {

	public Sample queryById(String id);
	
}
