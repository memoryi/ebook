package com.memoryi.ebook.web.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.memoryi.ebook.web.model.Sample;

public interface SampleMapper {

	@Select("select * from sys_users sample where sample.id = #{id}")
	public Sample queryById(@Param("id") String id);
	
}
