package com.memoryi.ebook.web.httpclient.factory;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.springframework.beans.factory.FactoryBean;

import com.memoryi.ebook.web.httpclient.strategy.TrustEverythingTrustStrategy;

public class HttpClientFactoryBean implements FactoryBean<HttpClient>{

	private String proxyHost = "127.0.0.1";
	private int proxyPort = 8080;
	private boolean proxyEnable = false;

	private boolean sslAllowAllEnable = false;

	private boolean cookieEnable = false;

	private int maxConn = 5;

	public void setProxyHost(String proxyHost) {
		this.proxyHost = proxyHost;
	}

	public void setProxyPort(int proxyPort) {
		this.proxyPort = proxyPort;
	}

	public void setMaxConn(int maxConn) {
		this.maxConn = maxConn;
	}

	@Override
	public HttpClient getObject() throws Exception {

		HttpClientBuilder builder = HttpClientBuilder.create();
		if (proxyEnable) {
			HttpHost proxy = new HttpHost(proxyHost, proxyPort);
			DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(
					proxy);
			builder.setRoutePlanner(routePlanner);
		}

		if (sslAllowAllEnable) {
			SSLContextBuilder context = new SSLContextBuilder();
			context.loadTrustMaterial(null, new TrustEverythingTrustStrategy());
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
					context.build(),
					SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			builder.setSSLSocketFactory(sslsf);
		}

		if (cookieEnable) {
			BasicCookieStore cookieStore = new BasicCookieStore();
			builder.setDefaultCookieStore(cookieStore);
		}

		HttpClient client = builder.setMaxConnTotal(maxConn).build();

		return client;

	}

	@Override
	public Class<HttpClient> getObjectType() {
		return HttpClient.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	public void setProxyEnable(boolean proxyEnable) {
		this.proxyEnable = proxyEnable;
	}

	public void setSslAllowAllEnable(boolean sslAllowAllEnable) {
		this.sslAllowAllEnable = sslAllowAllEnable;
	}

	public void setCookieEnable(boolean cookieEnable) {
		this.cookieEnable = cookieEnable;
	}
}
