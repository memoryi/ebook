package com.memoryi.ebook.web.config;


import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class DataSourceConfig {
	
	@Primary
	@Bean(initMethod="createPool")
	@ConfigurationProperties(prefix = DataSourceProperties.PREFIX)
    public DataSource dataSource() {
		
//		DataSource dataSource = new DataSource();
//		
//	    dataSource.setUrl(this.properties.getUrl());
//	    dataSource.setUsername(this.properties.getUsername());
//	    dataSource.setPassword(this.properties.getPassword());
//	    dataSource.setMaxIdle(this.properties.getMaxIdle());     
//	    dataSource.setMinIdle(this.properties.getMaxIdle());
//	    dataSource.setInitialSize(this.properties.getInitialSize());
		
	    return DataSourceBuilder.create().build();
    }

}
