package com.memoryi.ebook.web.config;
import org.apache.http.client.HttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.memoryi.ebook.web.httpclient.factory.HttpClientFactoryBean;

@Configuration
public class HttpClientConfig {

	@Value("${common.http.sslAllowAllEnable}")
	private boolean sslAllowAllEnable = false;
	
	@Bean
    public HttpClient httpClient() throws Exception {

		HttpClientFactoryBean factory = new HttpClientFactoryBean();
		
		factory.setSslAllowAllEnable(sslAllowAllEnable);
		
        return factory.getObject();
    }
}