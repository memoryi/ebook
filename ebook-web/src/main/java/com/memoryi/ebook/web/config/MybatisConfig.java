package com.memoryi.ebook.web.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan(value = "com.memoryi.ebook.web.mapper")
public class MybatisConfig {
}
